package gpg

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io"
	"os"
	"os/exec"
	"regexp"
	"strings"

	"golang.org/x/crypto/openpgp" //nolint:staticcheck // though this package is unmaintained, security fixes are still applied and it works for our needs
)

func GetEntities(files []string) (entities []*openpgp.Entity, err error) {
	// read armored file and store into entites array
	// the size of this array will be the number of shares
	for _, file := range files {
		// we import into gpg for use with encryption
		cmd := exec.Command("gpg", "--import", file)
		if _, err := cmd.CombinedOutput(); err != nil {
			return nil, err
		}

		entityList, err := getEntityListFromFile(file)
		if err != nil {
			return nil, err
		}

		entities = append(entities, entityList...)
	}

	return entities, nil
}

func ReadEntity(pubkey []byte) (result []string, err error) {
	var (
		cmd = exec.Command("gpg", "--with-colons", "--import-options", "show-only", "--import")
		out bytes.Buffer
	)

	cmd.Stdin = bytes.NewBuffer(pubkey)
	cmd.Stdout = &out

	if err := cmd.Run(); err != nil {
		return nil, err
	}

	sc := bufio.NewScanner(&out)
	for sc.Scan() {
		line := sc.Text()
		if strings.HasPrefix(line, "uid") {
			components := strings.Split(line, "::")
			if len(components) > 4 { //nolint:gomnd // This is a magical output parsing function and gets to be magic
				result = append(result, components[4])
			}
		}
	}

	return result, nil
}

// Import the given public key into the system's gpg keychain
func ImportPubkey(pubkey []byte) error {
	cmd := exec.Command("gpg", "--import")
	cmd.Stdin = bytes.NewBuffer(pubkey)

	return cmd.Run()
}

// GetEncryptionPacketKeyID by listing packets and parsing out the
// key that this message was encrypted to
func GetEncryptionPacketKeyID(message []byte) (string, error) {
	cmd := exec.Command("gpg", "--list-packets", "--list-only")
	cmd.Stdin = bytes.NewBuffer(message)

	stdout, err := cmd.Output()
	if err != nil {
		return "", err
	}

	sc := bufio.NewScanner(strings.NewReader(string(stdout)))
	for sc.Scan() {
		line := sc.Text()
		if strings.HasPrefix(line, ":pubkey enc packet") {
			components := strings.Split(line, " ")
			keyID := components[len(components)-1]

			if isValidKeyID(keyID) {
				return keyID, nil
			}
		}
	}

	return "", errors.New("key not found")
}

// GetEntityListFromFile returns EntityList after reading it from the armor keyring file
func getEntityListFromFile(keyringFile string) (openpgp.EntityList, error) {
	keyringReader, err := os.Open(keyringFile)
	if err != nil {
		return nil, err
	}

	defer keyringReader.Close()

	entityList, err := openpgp.ReadArmoredKeyRing(keyringReader)
	if err != nil {
		return nil, err
	}

	return entityList, nil
}

// SerializeWithoutSigs serializes the public part of the given Entity to w, excluding signatures from other entities
func SerializeWithoutSigs(entity *openpgp.Entity, w io.Writer) error {
	if err := entity.PrimaryKey.Serialize(w); err != nil {
		return err
	}

	for _, ident := range entity.Identities {
		if err := ident.UserId.Serialize(w); err != nil {
			return err
		}

		if err := ident.SelfSignature.Serialize(w); err != nil {
			return err
		}
	}

	for _, subkey := range entity.Subkeys {
		if err := subkey.PublicKey.Serialize(w); err != nil {
			return err
		}

		if err := subkey.Sig.Serialize(w); err != nil {
			return err
		}
	}

	return nil
}

// Look up a partial keyID in all fingerprints int he provided entity,
// returning the full fingerprint or an empty string if not found
func FingerprintFromKeyID(entity *openpgp.Entity, keyID string) string {
	if !isValidKeyID(keyID) {
		return ""
	}

	primaryFp := fmt.Sprintf("%X", entity.PrimaryKey.Fingerprint)
	if strings.HasSuffix(primaryFp, keyID) {
		return primaryFp
	}

	for _, sk := range entity.Subkeys {
		subkeyFp := fmt.Sprintf("%X", sk.PublicKey.Fingerprint)
		if strings.HasSuffix(subkeyFp, keyID) {
			return subkeyFp
		}
	}

	return ""
}

// Decrypt the given payload using the system's gpg keychain
func Decrypt(payload []byte, passphrase string) (stdout []byte, stderr bytes.Buffer, err error) {
	args := []string{"--decrypt"}
	if passphrase != "" {
		args = []string{
			"--pinentry-mode",
			"loopback",
			"--passphrase",
			passphrase,
			"--decrypt",
		}
	}

	cmd := exec.Command("gpg", args...)
	cmd.Stdin = bytes.NewReader(payload)
	cmd.Stderr = &stderr
	stdout, err = cmd.Output()

	return stdout, stderr, err
}

var keyIDRegex = regexp.MustCompile(`[0-9a-fA-F]{16}`)

func isValidKeyID(keyID string) bool {
	if len(keyID) != 16 {
		return false
	}

	loc := keyIDRegex.FindStringIndex(keyID)
	if len(loc) == 0 || loc[0] != 0 {
		return false
	}

	return true
}
