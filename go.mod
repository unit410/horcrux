module horcrux

go 1.21

require (
	github.com/google/subcommands v1.2.0
	gitlab.com/unit410/vault-shamir v0.0.0-20211221084242-1ab1ba782d74
	golang.org/x/crypto v0.12.0
)

require github.com/hashicorp/errwrap v1.1.0 // indirect
