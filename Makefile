# Ref: https://hub.docker.com/_/golang
build_image="golang:1.21-bookworm@sha256:12359f96c43077803959dab88ce60c87cd2543f3b907bf54a34a5104270f404f"
# Paths
mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
mkfile_dir := $(dir $(mkfile_path))
GO_SRC = go.mod go.sum $(shell find . -type f -name '*.go')

all: build

build: build/horcrux-linux-amd64 build/horcrux-darwin-amd64 build/horcrux-darwin-arm64

build/horcrux-linux-amd64:
	GOOS=linux  GOARCH=amd64 CGO_ENABLED=0 go build -trimpath -o ./build/horcrux-linux  ./cmd/horcrux
build/horcrux-darwin-amd64:
	GOOS=darwin GOARCH=amd64 CGO_ENABLED=0 go build -trimpath -o ./build/horcrux-darwin-amd64 ./cmd/horcrux
build/horcrux-darwin-arm64:
	GOOS=darwin GOARCH=arm64 CGO_ENABLED=0 go build -trimpath -o ./build/horcrux-darwin-arm64  ./cmd/horcrux

build-release: clean lint
	docker run --rm \
		--platform "linux/amd64" \
		--volume $(mkfile_dir):/build \
		--workdir /build \
		$(build_image) \
		make build/horcrux-linux-amd64

coverage: cover.out ## Run code coverage tests

cover.out: $(GO_SRC)
	CGO_ENABLED=0 go test $$(go list ./... | grep -v /vendor/) -coverprofile cover.out
	go tool cover -func cover.out

coverage.xml: cover.out ## Run code coverage with XML output for CI
	@$(MAKE) deps-dev
	gocover-cobertura < cover.out > coverage.xml

deps-dev:
	@command -v gotestsum >/dev/null || go install gotest.tools/gotestsum@latest
	@command -v golangci-lint || go install github.com/golangci/golangci-lint/cmd/golangci-lint@latest
	@command -v gocover-cobertura >/dev/null || go install github.com/boumenot/gocover-cobertura@latest

lint: deps-dev
	go mod verify
	go fmt $$(go list ./... | grep -v /vendor/)
	golangci-lint run -c ./.golangci.yml

test: lint
	go test $$(go list ./... | grep -v /vendor/)

test.xml: ## Run test suite with XML output for CI
	@$(MAKE) deps-dev
	gotestsum --junitfile test.xml --format testname

integration:
	./test/integration

clean:
	rm -rf build

.PHONY: all
